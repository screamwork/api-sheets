<?php

	namespace App\PhpTrait;

	use PhpOffice\PhpSpreadsheet\IOFactory;

	trait SaveSpreadSheet {

		public function saveSpreadSheet() {

			try {
				if($this->fileParts[1] === "xlsx") {
					$this->writer = IOFactory::createWriter($this->spreadsheet, "Xlsx");
					$this->writer->save($this->outputFile);
				} elseif($this->fileParts[1] === "csv") {
					$this->writer = IOFactory::createWriter($this->spreadsheet, "Xlsx");
					$this->writer->save($this->outputFile);
				}
			} catch (\Exception $e) {
				throw new \Exception($e->getMessage());
			}

			return;
		}
	}