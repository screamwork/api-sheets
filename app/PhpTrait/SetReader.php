<?php

	namespace App\PhpTrait;

	use PhpOffice\PhpSpreadsheet\IOFactory;

	trait SetReader {

		public function setReader() {
			try {
				if($this->fileParts[1] === "csv") {
					$this->reader = IOFactory::createReader("Xlsx");
					$this->spreadsheet = $this->reader->load($this->outputFile);
				} elseif($this->fileParts[1] === "xlsx") {
					$this->reader = IOFactory::createReader("Xlsx");
					$this->spreadsheet = $this->reader->load($this->inputFile);
				}
			} catch(\Exception $e) {
				throw new \Exception($e->getMessage());
			}
			return;
		}
	}