<?php

	namespace App\PhpTrait;

	use PhpOffice\PhpSpreadsheet\Shared\Date;

	trait ReadXlsx {

		public function readXlsx($sheetName) {
			$sheet = $this->spreadsheet->getSheetByName($sheetName);

			foreach ($sheet->getRowIterator() as $row) {
				$cellIterator = $row->getCellIterator();
				$cellIterator->setIterateOnlyExistingCells(true);

				$arr = [];
				foreach ($cellIterator as $cell) {
					if ($cell->getValue() === 'Date' || $cell->getValue() === 'Link') {
						continue;
					} else {
						if (preg_match("/^A/", $cell->getCoordinate())) {
							$arr[] = date("m/d/y", Date::excelToTimeStamp($cell->getValue()));
						} else {
							$arr[] = $cell->getValue();
						}
					}
				}

				if (!$arr) {
					continue;
				}

				if ($arr && count($arr)) {
					$this->process($arr, $row->getRowIndex());
				}
			}
			return;
		}
	}
