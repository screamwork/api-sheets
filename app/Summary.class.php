<?php

	namespace App;

	use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
	use PhpOffice\PhpSpreadsheet\Shared\Date;
	use Requests;


	use App\PhpTrait\SaveSpreadSheet;
	use App\PhpTrait\SetReader;

	class Summary	{
		use SaveSpreadSheet, SetReader;

		private $inputFile;
		private $outputFile;
		private $row;
		private $sheetName;
		private $writer;
		private $reader;
		private $alphabet;
		private $fileParts;
		private $spreadsheet;
		private $inputSheet;
		private $classSheet;
		private $filename;
		private $postmarkApiKey;
		private $outputPath;
		private $emailFrom;
		Private $emailTo;
		private $fieldsToCopy;
		private $pageVisitSheet;

		public function __construct($postmarkApiKey, $fieldsToCopy, $inputFile, $outputFile, $outputPath, $emailFrom, $emailTo, $sheetName) {
			if(!$sheetName) {
				throw new \Exception("no sheetName");
			}

			$this->sheetName = $sheetName;
			$this->outputFile = $outputFile;
			$this->inputFile = $inputFile;
			$this->alphabet = range('A', 'Z');
			$this->row = 1;
			$this->fileParts = explode(".", $this->inputFile);
			$this->filename = substr($this->fileParts[0], strrpos($this->fileParts[0], '/') + 1);
			$this->postmarkApiKey = $postmarkApiKey;
			$this->outputPath = $outputPath;
			$this->emailFrom = $emailFrom;
			$this->emailTo = $emailTo;
			$this->fieldsToCopy = $fieldsToCopy;

			$this->setReader();

			try {
        $this->classSheet = new Worksheet($this->spreadsheet, $this->sheetName);
        $this->inputSheet = new Worksheet($this->spreadsheet, "Input");
        $this->pageVisitSheet = new Worksheet($this->spreadsheet, "Page Visits by Date");
			} catch(\Exception $e) {
				throw new \Exception($e->getMessage());
			}

			if($this->fileParts[1] === "csv") {
				$this->copyCsv();
        $this->spreadsheet->addSheet($this->classSheet);
			} elseif($this->fileParts[1] === "xlsx") {
				$this->worksheetClone();
        $sheetIndex = $this->spreadsheet->getIndex(
          $this->spreadsheet->getSheetByName('sample')
        );
        $this->spreadsheet->removeSheetByIndex($sheetIndex);
      }
      $this->createInputSheet();
			$this->createPageVisitSheet();
      $this->spreadsheet->addSheet($this->inputSheet);
      $this->spreadsheet->addSheet($this->pageVisitSheet);

      $this->saveSpreadSheet();
			#$trySend = $this->sendEmail();
			#print '<pre>'.print_r($trySend, true).'</pre>';
		}

		public function createPageVisitSheet() {
      $articleSheet = $this->spreadsheet->getSheetByName("Article");
      $pageViewSheet = $this->spreadsheet->getSheetByName("PageView");
      $pageViewArr = $pageViewSheet->toArray();
      $this->pageVisitSheet->setCellValue("D1", "visits");

      foreach ($articleSheet->getRowIterator() as $aRow) {
        $aCellIterator = $aRow->getCellIterator();
        $aCellIterator->setIterateOnlyExistingCells(true);

        foreach ($aCellIterator as $aCell) {
          $needed = ["A", "B", "C"];
          if($aRow->getRowIndex() === 1) {
            if(in_array($aCell->getColumn(), $needed)) {
              $this->pageVisitSheet->setCellValue($aCell->getCoordinate(), $aCell->getValue());
            }
          }
          if($aRow->getRowIndex() > 1) {
            // get ID cell.
            if($aCell->getColumn() === "A") {
              $id = $aCell->getValue();
            }
            // check only cells we need.
            if(in_array($aCell->getColumn(), $needed)) {
              foreach($pageViewArr as $pvi) {
                if($pvi[0] === $id && $pvi[1] === $aCell->getValue()) {
                  $this->pageVisitSheet->setCellValue("D{$aRow->getRowIndex()}", $pvi[3]);
                }
              }
              // the date field needs special threatment.
              if($aCell->getColumn() === "B") {
                if($aCell->getDataType() != "s") {
                  $this->pageVisitSheet->setCellValue("{$aCell->getColumn()}{$aRow->getRowIndex()}", date("m/d/y", Date::excelToTimeStamp($aCell->getValue())));
                } else {
                  $this->pageVisitSheet->setCellValue($aCell->getCoordinate(), $aCell->getValue());
                }
              } else {
                $this->pageVisitSheet->setCellValue($aCell->getCoordinate(), $aCell->getValue());
              }
            }
          }
        }
      }
    }

		function sendEmail() {

			$htmlBody = "<html><body><h2>New API-Spreadsheet in Attachment</h2></body></html>";
			$type = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

			$filename = $this->filename . "_" .date("d-m-y_H:i:s");
			$renamed =  $this->outputPath . $filename . ".xlsx";

			shell_exec("mv " . $this->outputFile . " " . $renamed);

			$headers = [
				"Content-Type" => "application/json",
				"Accept" => "application/json",
				"X-Postmark-Server-Token" => $this->postmarkApiKey
			];
			$options = [
				"From" => $this->emailFrom,
					"To" => $this->emailTo,
					"Subject" => "Attachment",
					"HtmlBody" => $htmlBody,
					"Attachments" => [
				    [
				      "Name" => $filename,
				      "Content" => base64_encode(file_get_contents($renamed)),
				      "ContentType" => $type
				    ]
				  ]
			];

			$request = Requests::post('https://api.postmarkapp.com/email', $headers, json_encode($options));
			// json_decode !
			return $request->body;
		}

		public function createInputSheet() {
			$sheet = $this->spreadsheet->getSheetByName($this->sheetName);
      $neededCells = [];

			foreach ($sheet->getRowIterator() as $row) {
				$cellIterator = $row->getCellIterator();
				$cellIterator->setIterateOnlyExistingCells(true);

        foreach ($cellIterator as $cell) {
				  if($row->getRowIndex() === 1) {
						if(in_array(strtolower($cell->getValue()), $this->fieldsToCopy)) {
							if(strtolower($cell->getValue()) === "date") {
								$dataColIdx = $cell->getColumn();
							}
							$neededCells[] = $cell->getColumn();
							$this->inputSheet->setCellValue("{$cell->getColumn()}{$row->getRowIndex()}", $cell->getValue());
						}
					}

          if($row->getRowIndex() > 1) {
            if($cell->getValue() && in_array($cell->getColumn(), $neededCells)) {
              if($cell->getDataType() != "s" && $cell->getColumn() === $dataColIdx) {
                $this->inputSheet->setCellValue("{$cell->getColumn()}{$row->getRowIndex()}", date("m/d/y", Date::excelToTimeStamp($cell->getValue())));
              }
              else {
                $this->inputSheet->setCellValue("{$cell->getColumn()}{$row->getRowIndex()}", $cell->getValue());
              }
            }
          }
        }
			}
		}

		public function worksheetClone() {
			$this->spreadsheet = $this->reader->load($this->inputFile);
			$clonedWorksheet = clone $this->spreadsheet->getSheetByName($this->filename);
			$clonedWorksheet->setTitle($this->sheetName);
			$this->spreadsheet->addSheet($clonedWorksheet);
			return;
		}

		public function copyCsv() {
			if (($handle = fopen($this->inputFile, "r")) !== false) {
				while (($data = fgetcsv($handle, 1000, ",")) !== false) {
					$num = count($data);

					$arr = [];
					for ($c = 0; $c < $num; $c++) {
						$arr[] = $data[$c];
					}

					if ($arr && count($arr)) {

						$colCount = 0;
						foreach($arr as $value) {
							$cell = $this->alphabet[$colCount] . $this->row;
							$this->classSheet->setCellValue($cell, $value);
							$colCount++;
						}
						$this->row += 1;
					}
				}
			}
			return;
		}
	}