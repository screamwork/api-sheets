<?php

	namespace App;

	use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

	use App\PhpTrait\ReadXlsx;
	use App\PhpTrait\ReadCsv;
	use App\PhpTrait\SetSpreadSheetValue;
	use App\PhpTrait\SaveSpreadSheet;
	use App\PhpTrait\SetReader;

	class Article {
		use ReadXlsx, ReadCsv, SetSpreadSheetValue, SaveSpreadSheet, SetReader;

		public $spreadsheet;
		public $classSheet;
		public $alphabet;
		public $apiKey;
		public $inputFile;
		public $outputFile;
		public $row;
		public $sheetName;
		public $writer;
		public $reader;
		public $querySheet;
		public $queryRow;
		public $filename;
		public $fileParts;

		public function __construct($apiKey, $inputFile, $outputFile, $sheetName) {

			$this->sheetName = $sheetName;
			$this->apiKey = $apiKey;
			$this->inputFile = $inputFile;
			$this->outputFile = $outputFile;
			$this->alphabet = range('A', 'Z');
			$this->row = 1;
			$this->queryRow = 1;
			$this->classSheet = new Worksheet($this->spreadsheet, $this->sheetName);
			$this->querySheet = new Worksheet($this->spreadsheet, "Query");
			$this->fileParts = explode(".", $this->inputFile);
			$this->filename = substr($this->fileParts[0], strrpos($this->fileParts[0], '/') + 1);

			try {

				$this->setReader();

				if($this->spreadsheet->sheetNameExists($this->sheetName)) {
					$sheetIndex = $this->spreadsheet->getIndex(
						$this->spreadsheet->getSheetByName($this->sheetName)
					);
					$this->spreadsheet->removeSheetByIndex($sheetIndex);
					$this->spreadsheet->addSheet($this->classSheet);
				} else {
					$this->spreadsheet->addSheet($this->classSheet);
				}

			} catch(\Exception $e) {
				throw new \Exception($e->getMessage());
			}

			if($this->fileParts[1] === "csv") {
				$this->readCsv();
			} elseif($this->fileParts[1] === "xlsx") {
				$this->readXlsx($this->filename);
			}

			$this->saveSpreadSheet();
		}

		public function process($arr, $currentId) {
			if(count($arr) !== 2 || !$arr[0] || !$arr[1]) {
				return;
			}

			$date = strtotime($arr[0]) * 1000;
//			$startDate = strtotime("-3 day", $date);
      $startDate = $date;
			$endDate = strtotime("+7 day", $date);
			$parse = (object) parse_url($arr[1]);
			if(!property_exists($parse, 'host') || $parse->host === "") {
				return;
			}
			$filter = json_encode(["href:\"$arr[1]\"", "domain:\"$parse->host\""]);
			$json = $this->get_a_curl($filter, $startDate, $endDate);

			sleep(1);

			if($json && !empty($json)) {

				foreach($json as $d) {
					$needed = [
						'id'             => $currentId,
						'date'           => $arr[0],
						'link'           => $arr[1],
						'facebook_count' => !empty($d->stats->fb_total->count) ? $d->stats->fb_total->count : "N/A",
						'facebook_total' => !empty($d->stats->fb_total->sum)   ? $d->stats->fb_total->sum : "N/A",
						'twitter_count'  => !empty($d->stats->twitter->count)  ? $d->stats->twitter->count: "N/A",
						'twitter_total'  => !empty($d->stats->twitter->sum)    ? $d->stats->twitter->sum: "N/A",
						'linkedIn_count' => !empty($d->stats->likedin->count)  ? $d->stats->likedin->count : "N/A",
						'linkedIn_total' => !empty($d->stats->likedin->sum)    ? $d->stats->likedin->sum : "N/A",
					];
					$this->setSpreadSheetValues($needed);
				}

				/* query tab */
				$queryArr = [
					'id'           => $currentId,
					'link_href'    => $arr[1],
					'domain_href'  => "",
				];

				$this->doQueryTab($queryArr);

			} else {

				$now = strtotime(date("m/d/y"));
				$weekAgo = date("m/d/y", strtotime("-7 day", $now));

				$needed = [
					'id'             => $currentId,
					'date'           => $weekAgo,
					'link'           => $arr[1],
					'facebook_count' => "N/A",
					'facebook_total' => "N/A",
					'twitter_count'  => "N/A",
					'twitter_total'  => "N/A",
					'linkedIn_count' => "N/A",
					'linkedIn_total' => "N/A",
				];

				/* query tab */
				$queryArr = [
					'id'           => $currentId,
					'link_href'    => $arr[1],
					'domain_href'  => "",
				];

				$this->doQueryTab($queryArr);
				$this->setSpreadSheetValues($needed);

			}
			return;
		}

		public function doQueryTab($queryData) {

			try {
				if (!$this->spreadsheet->sheetNameExists("Query")) {
					$this->spreadsheet->addSheet($this->querySheet);
				}
				else {
					$this->querySheet = $this->spreadsheet->getSheetByName("Query");
				}
			} catch(\Exception $e) {
				throw new \Exception($e->getMessage());
			}

			/* query headings */
			if($this->queryRow === 1) {
				$colCount = 0;
				foreach($queryData as $needKey => $needVal) {
					$cell = $this->alphabet[$colCount] . $this->queryRow;
					try{
						$this->querySheet->setCellValue($cell, $needKey);
					} catch(\Exception $e) {
						echo new \Exception($e->getMessage());
					}
					$colCount++;
				}
				$this->queryRow += 1;
			}

			/* query body */
			$colCount = 0;
			foreach($queryData as $need) {
				try{
					$this->querySheet->setCellValue($this->alphabet[$colCount] . $this->queryRow, $need);
				} catch(\Exception $e) {
					echo new \Exception($e->getMessage());
				}
				$colCount++;
			}
			$this->queryRow += 1;
			return;
		}

		public function get_a_curl($filter, $from, $to) {

			$curl_articles = "
				curl -H \"Content-Type: application/json\" -X POST -d '{
					\"filters\": $filter,
					\"sort_by\": \"fb_total.sum\",
					\"aggregate_by\": \"domain\",
					\"from\": $from,
					\"to\": $to,
					\"size\": 5000
				}' \"https://api.newswhip.com/v1/stats?key=$this->apiKey\"
			";

			$exec = shell_exec($curl_articles);
			$json = json_decode($exec);

			if($json && is_object($json) && !empty($json->articles)) {
				return $json;
			} else {
				$curl_articles_no_date = "
					curl -H \"Content-Type: application/json\" -X POST -d '{
						\"filters\": $filter,
						\"sort_by\": \"fb_total.sum\",
						\"aggregate_by\": \"domain\",
						\"size\": 5000
					}' \"https://api.newswhip.com/v1/stats?key=$this->apiKey\"
				";

				$exec1 = shell_exec($curl_articles_no_date);
				return json_decode($exec1);
			}
		}
	}
