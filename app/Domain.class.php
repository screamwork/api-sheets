<?php

	namespace App;

	use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
	use PhpOffice\PhpSpreadsheet\IOFactory;

	use App\PhpTrait\ReadXlsx;
	use App\PhpTrait\ReadCsv;
	use App\PhpTrait\SetSpreadSheetValue;
	use App\PhpTrait\SaveSpreadSheet;
	use App\PhpTrait\SetReader;

	class Domain {
		use ReadXlsx, ReadCsv, SetSpreadSheetValue, SaveSpreadSheet, SetReader;

		private $spreadsheet;
		private $classSheet;
		private $alphabet;
		private $apiKey;
		private $inputFile;
		private $outputFile;
		private $row;
		private $sheetName;
		private $writer;
		private $reader;
		private $querySheet;
		private $queryRow;
		public $filename;
		public $fileParts;

		public function __construct($apiKey, $inputFile, $outputFile,	$sheetName) {
			if(!$sheetName) {
				throw new \Exception("no sheetName");
			}

			$this->sheetName = $sheetName;
			$this->apiKey = $apiKey;
			$this->inputFile = $inputFile;
			$this->outputFile = $outputFile;
			$this->alphabet = range( 'A', 'Z' );
			$this->row = 1;
			$this->queryRow = 1;
			$this->classSheet = new Worksheet($this->spreadsheet, $this->sheetName);
			$this->querySheet = new Worksheet($this->spreadsheet, "Query");
			$this->fileParts = explode(".", $this->inputFile);
			$this->filename = substr($this->fileParts[0], strrpos($this->fileParts[0], '/') + 1);

			try {
				$this->setReader();

				if($this->spreadsheet->sheetNameExists($this->sheetName)) {
					$sheetIndex = $this->spreadsheet->getIndex(
						$this->spreadsheet->getSheetByName($this->sheetName)
					);
					$this->spreadsheet->removeSheetByIndex($sheetIndex);
					$this->spreadsheet->addSheet($this->classSheet);
				} else {
					$this->spreadsheet->addSheet($this->classSheet);
				}

			} catch(\Exception $e) {
				throw new \Exception($e->getMessage());
			}

			if($this->fileParts[1] === "csv") {
				$this->readCsv();
			} elseif($this->fileParts[1] === "xlsx") {
				$this->readXlsx($this->filename);
			}

			$this->saveSpreadSheet();
		}

		public function process($arr, $currentId) {
			if(count($arr) !== 2 || !$arr[0] || !$arr[1]) {
				return;
			}

			$parse = (object) parse_url($arr[1]);
			if(!property_exists($parse, 'host') || $parse->host === "") {
				return;
			}
			$exp = explode(".", $parse->host);

			if(count($exp) === 3) {
				$host = $exp[1] . '.' . $exp[2];
			} elseif(count($exp) === 2) {
				$host = $exp[0] . '.' . $exp[1];
			} else {
				$host = $exp[0];
			}

			$date = strtotime($arr[0]) * 1000;
			$startDate = strtotime("-3 day", $date);
			$endDate = strtotime("+3 day", $date);
			$filter = json_encode("domain:\"$host\"");
			$json = $this->get_a_curl($filter, $startDate, $endDate);

			sleep(1);

			if($json && is_object($json) && !empty($json->articles)) {

				$needed = [
					'id'            => $currentId,
					'date'          => $arr[0], // date('Y-m-d', strtotime($arr[0])),
					'link'          => $arr[1],
					'domain'        => $parse->host,
					'article_count' => count($json->articles)
				];

				/* query tab */
				$queryArr = [
					'domain_href' => $host,
          'date'        => $arr[0],
				];

				$this->doQueryTab($queryArr);
				$this->setSpreadSheetValues($needed);

			} else {
				$now = strtotime(date("m/d/y"));
				$weekAgo = date("m/d/y", strtotime("-7 day", $now));

				$needed = [
					'id'            => $currentId,
					'date'          => $weekAgo,
					'link'          => $arr[1],
					'domain'        => $parse->host,
					'article_count' => "N/A"
				];

				/* query tab */
				$queryArr = [
					'domain_href' => $host,
          'date'        => $arr[0],
				];

				$this->doQueryTab($queryArr);
				$this->setSpreadSheetValues($needed);

			}
		}

		public function doQueryTab($queryData) {

			try {
				if (!$this->spreadsheet->sheetNameExists("Query")) {
					$this->spreadsheet->addSheet($this->querySheet);
          $this->querySheet->setCellValue("D3", "date");
				}
				else {
					$this->querySheet = $this->spreadsheet->getSheetByName("Query");
				}
			} catch(\Exception $e) {
				throw new \Exception($e->getMessage());
			}

			/* query body */
      $count = 2;
			foreach($queryData as $need) {
				if($this->queryRow === 1) {
					$this->queryRow++;
				}
				$cell = $this->alphabet[$count] . $this->queryRow;

				try{
					$this->querySheet->setCellValue($cell, $need);
				} catch(\Exception $e) {
					echo new \Exception($e->getMessage());
				}
				$count++;
			}
			$this->queryRow += 1;
			return;
		}

		public function get_a_curl($filter, $from, $to) {

			$curl_articles = "
				curl -H \"Content-Type: application/json\" -X POST -d '{
					\"filters\": [$filter],
					\"from\": $from,
					\"to\": $to,
					\"size\": 5000
				}' \"https://api.newswhip.com/v1/articles?key=$this->apiKey\"
			";

			$exec = shell_exec($curl_articles);
			$json = json_decode($exec);

			if($json && is_object($json) && !empty($json->articles)) {
				return $json;
			} else {
				$curl_articles_no_date = "
					curl -H \"Content-Type: application/json\" -X POST -d '{
						\"filters\": [$filter],
						\"size\": 5000
					}' \"https://api.newswhip.com/v1/articles?key=$this->apiKey\"
				";

				$exec1 = shell_exec($curl_articles_no_date);
				return json_decode($exec1);
			}
		}
	}
