<?php

	namespace App;

	use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

	use App\PhpTrait\ReadXlsx;
	use App\PhpTrait\ReadCsv;
	use App\PhpTrait\SetSpreadSheetValue;
	use App\PhpTrait\SaveSpreadSheet;
	use App\PhpTrait\SetReader;

	class Gender {
		use ReadXlsx, ReadCsv, SetSpreadSheetValue, SaveSpreadSheet, SetReader;

		private $apiKeySim;
		private $inputFile;
		private $outputFile;
		private $classSheet;
		private $row;
		private $sheetName;
		private $writer;
		private $reader;
		private $spreadsheet;
		private $alphabet;
		private $filename;
		private $fileParts;

		public function __construct($apiKey, $inputFile, $outputFile,	$sheetName) {
			if(!$sheetName) {
				throw new \Exception("no sheetName");
			}

			$this->apiKeySim  = $apiKey;
			$this->inputFile = $inputFile;
			$this->sheetName = $sheetName;
			$this->outputFile = $outputFile;
			$this->alphabet = range( 'A', 'Z' );
			$this->row = 1;
			$this->classSheet = new Worksheet($this->spreadsheet, $this->sheetName);
			$this->fileParts = explode(".", $this->inputFile);
			$this->filename = substr($this->fileParts[0], strrpos($this->fileParts[0], '/') + 1);

			$this->setReader();

			if($this->spreadsheet->sheetNameExists($this->sheetName)) {
				$sheetIndex = $this->spreadsheet->getIndex(
					$this->spreadsheet->getSheetByName($this->sheetName)
				);
				$this->spreadsheet->removeSheetByIndex($sheetIndex);
				$this->spreadsheet->addSheet($this->classSheet);
			} else {
				$this->spreadsheet->addSheet($this->classSheet);
			}

			if($this->fileParts[1] === "csv") {
				$this->readCsv();
			} elseif($this->fileParts[1] === "xlsx") {
				$this->readXlsx($this->filename);
			}

			$this->saveSpreadSheet();
		}

		public function process($arr, $currentId) {
			if(count($arr) !== 2 || !$arr[0] || !$arr[1]) {
				return;
			}

			$parse = (object) parse_url($arr[1]);
			if(!property_exists($parse, 'host') || $parse->host === "") {
				return;
			}
			$date = strtotime($arr[0]);
			$startDate = date("Y-m", strtotime("-3 day", $date));
			$endDate = date("Y-m", strtotime("+3 day", $date));

			$url = "https://api.similarweb.com/v1/website/***/demographics/gender?" .
					"format=json&".
					"api_key=$this->apiKeySim&".
					"country=us&".
					"start_date=$startDate&".
					"end_date=$endDate&".
					"main_domain_only=false&".
					"granularity=monthly&".
					"limit=0"
			;

			$url = str_replace('***', $parse->host, $url);
			$json = $this->get_a_curl($url);

			if ($json && is_object($json) && !empty($json) && !property_exists($json->meta,"error_code")) {

				$needed = [
					'id' => $currentId,
					'link' => $arr[1],
					'domain' => $parse->host,
					'male' => $json->male,
					'female' => $json->female
				];

				$this->setSpreadSheetValues($needed);

			} else {
				$needed = [
					'id' => $currentId,
					'link' => $arr[1],
					'domain' => $parse->host,
					'male' => "N/A",
					'female' => "N/A"
				];

				$this->setSpreadSheetValues($needed);
			}
		}

		public function get_a_curl($url) {
			$curl = curl_init();
			curl_setopt_array($curl, array(
			  CURLOPT_URL => $url,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => false,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			));
			$response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);

			if ($err) {
			  var_dump("cURL Error #:" . $err);
			} else {
			  return json_decode($response);
			}
		}
	}