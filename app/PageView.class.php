<?php

	namespace App;

	use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
	use PhpOffice\PhpSpreadsheet\IOFactory;

	use App\PhpTrait\ReadXlsx;
	use App\PhpTrait\ReadCsv;
	use App\PhpTrait\SetSpreadSheetValue;
	use App\PhpTrait\SaveSpreadSheet;
	use App\PhpTrait\SetReader;

	class PageView {
		use ReadXlsx, ReadCsv, SetSpreadSheetValue, SaveSpreadSheet, SetReader;

		private $apiKeySimilarWeb;
		private $inputFile;
		private $classSheet;
		private $row;
		private $sheetName;
		private $writer;
		private $reader;
		private $outputFile;
		private $spreadsheet;
		private $alphabet;
		private $filename;
		private $fileParts;

		public function __construct($apiKey = "CmAHq1F6wW7UQ", $inputFile = 'sample.csv', $outputFile = "results.xlsx", $sheetName = false) {
			if(!$sheetName) {
				throw new \Exception("no sheetName");
			}
			$this->sheetName = $sheetName;
			$this->outputFile = $outputFile;
			$this->apiKeySimilarWeb  = $apiKey;
			$this->inputFile = $inputFile;
			$this->alphabet = range( 'A', 'Z' );
			$this->row = 1;
			$this->classSheet = new Worksheet($this->spreadsheet, $this->sheetName);
			$this->fileParts = explode(".", $this->inputFile);
			$this->filename = substr($this->fileParts[0], strrpos($this->fileParts[0], '/') + 1);

			$this->setReader();

			if($this->spreadsheet->sheetNameExists($this->sheetName)) {
				$sheetIndex = $this->spreadsheet->getIndex(
					$this->spreadsheet->getSheetByName($this->sheetName)
				);
				$this->spreadsheet->removeSheetByIndex($sheetIndex);
				$this->spreadsheet->addSheet($this->classSheet);
			} else {
				$this->spreadsheet->addSheet($this->classSheet);
			}

			if($this->fileParts[1] === "csv") {
				$this->readCsv();
			} elseif($this->fileParts[1] === "xlsx") {
				$this->readXlsx($this->filename);
			}

			$this->saveSpreadSheet();
		}

		public function process($arr, $currentId)	{
			if(count($arr) !== 2 || !$arr[0] || !$arr[1]) {
				return;
			}
			$parse = (object) parse_url($arr[1]);
			if(!property_exists($parse, 'host') || $parse->host === "") {
				return;
			}
			$date = strtotime($arr[0]);
			$startDate = date("Y-m", strtotime("-3 day", $date));
			$endDate = date("Y-m", strtotime("+3 day", $date));


			$exp = explode(".", $parse->host);
			if(count($exp) ===3) {
				$domain = $exp[1] . '.' . $exp[2];
			} elseif(count($exp) ===2) {
				$domain = $exp[0] . '.' . $exp[1];
			} elseif(count($exp) ===1) {
				$domain = $exp[1];
			}

			$url = "https://api.similarweb.com/v1/website/***/traffic-and-engagement/visits?" .
				"format=json&".
				"api_key=$this->apiKeySimilarWeb&".
				"country=us&".
				"start_date=$startDate&".
				"end_date=$endDate&".
				"main_domain_only=false&".
				"granularity=daily"
			;
			$url = str_replace('***', $domain, $url);
			$json = $this->get_a_curl($url);

			if ($json && is_object($json) && property_exists($json->meta, 'status') && $json->meta->status === "Success") {

				foreach($json->visits as $v) {
					$needed = [
						'id' => $currentId,
						'date' => date("m/d/y", strtotime($v->date)),
						'link' => $domain,
						'visits' => $v->visits,
					];
					$this->setSpreadSheetValues($needed);
				}

			} else {

				$needed = [
					'id'     => $currentId,
					'date'   => $arr[0],
					'link'   => $arr[1],
					'visits' => "N/A",
				];
				$this->setSpreadSheetValues($needed);
			}
		}

		public function get_a_curl($url) {
			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => false,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
				var_dump("cURL Error #:" . $err); exit('problem');
			} else {
				return json_decode($response);
			}
		}
	}