<?php

	require 'vendor/autoload.php';

	use App\Article;
	use App\Domain;
	use App\Gender;
	use App\Age;
	use App\PageView;
	use App\Summary;

	/******* variables to edit *******++++++++++++++++++***/
  $fieldsToCopy = ["date", "link"];
	$file = "sample.xlsx";
	$apiKeyNewsWhip = "<news_whip_api_key>";
	$apiKeySimilarWeb = "<similar_web_api_key>";
	$postmarkApiKey = "<postmark_api_key>";
	$emailTo = "<to>";
	$emailFrom = "<from>";
	/******* stop edit below +++++++++++++++++++**********/

	$start = date('H:i:s');
	$inputPath = dirname(__FILE__) . DIRECTORY_SEPARATOR . "input" . DIRECTORY_SEPARATOR;
	$outputPath = dirname(__FILE__) . DIRECTORY_SEPARATOR . "output" . DIRECTORY_SEPARATOR;
	$absInput = $inputPath . $file;
	$absOutput = $outputPath . $file;
	$fileParts = explode(".", $file);

	// check that output file is in place
	if($fileParts[1] === "csv") {
		if(!file_exists($outputPath . $file)) {
			$explodedFile = explode('.', $file);
			shell_exec("touch " . $outputPath . $explodedFile[0] . ".xlsx");
			$absInput = $inputPath . $file;
			$absOutput = $outputPath . $explodedFile[0] . ".xlsx";
		}
	} elseif($fileParts[1] === "xlsx") {
		if(!file_exists($outputPath . $file)) {
			shell_exec("cp " . $inputPath . $file . " " . $outputPath . $file);
			$absInput = $outputPath . $file;
		}
	}

	try {
		new Article($apiKeyNewsWhip, $absInput, $absOutput, "Article");
		new Domain($apiKeyNewsWhip, $absInput, $absOutput, "Domain");
		new Gender($apiKeySimilarWeb, $absInput, $absOutput, "Gender");
		new Age($apiKeySimilarWeb, $absInput, $absOutput, "Age");
		new PageView($apiKeySimilarWeb, $absInput, $absOutput, "PageView");
		new Summary($postmarkApiKey, $fieldsToCopy, $absInput, $absOutput, $outputPath, $emailFrom, $emailTo, "Summary");

	} catch(\Exception $e) {
		throw new \Exception($e->getMessage());
	}

	$end = date('H:i:s');
	echo abs(strtotime($end) - strtotime($start)) . 'sec';
  shell_exec("mv ".$absOutput." ".$outputPath.$fileParts[0]."-".date("Y-m-d-H:i:s").".xlsx");
